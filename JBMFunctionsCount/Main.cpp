
#include <iostream>
#include <conio.h>

using namespace std;

float Add(float num1, float num2)
{
	float add;
	add = num1 + num2;
	return add;
}
float Subtract(float num1, float num2)
{
	float subtract;
	subtract = num1 - num2;
	return subtract;
}
float Multiply(float num1, float num2)
{
	float multiply;
	multiply = num1 * num2;
	return multiply;

}
float Divide(float num1, float num2)
{
	float divide;
	divide = num1 / num2;
	return divide;
}

int main()
{
	char changes;
	int x = 1;
	char again = 'y';
	float num1, num2;
	char ans;
	while (again == 'y')
		if (x == 1)
		{
			cout << "Please enter the first number: \n";
			cin >> num1;
			cout << "Please enter the second number: \n";
			cin >> num2;
			if (num2 == 0)
			{
				cout << "Cannot devide by zero\n";
				cout << "defaulting num2 to 1\n";
				num2 = 1;
			}

			cout << "Now enter the corresponding operation you desire to do(+ , -, *, /)\n";
			cin >> ans;

			if (ans == '+')
			{
				cout << num1 << " + " << num2 << " = " << Add(num1, num2) << "\n";
			}
			else if (ans == '-')
			{
				cout << num1 << "-" << num2 << "=" << Subtract(num1, num2) << "\n";
			}
			else if (ans == '*')
			{
				cout << num1 << "*" << num2 << "=" << Multiply(num1, num2) << "\n";
			}
			else if (ans == '/')
			{
				cout << num1 << "/" << num2 << "=" << Divide(num1, num2) << "\n";
			}
			else
			{
				cout << "Invalid choice\n";
			}
			cout << "Would you like to make another equation(y/n)?\n";
			cin >> again;
		}

	_getch();
	return 0;
}
